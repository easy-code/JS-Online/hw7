##### Cсылка на видео
[Video](https://drive.google.com/drive/folders/1LFEZblqzqgHn01zaPH_hdyhEPXDlAU3g)

##### Ссылки на презентацию
[First](https://docs.google.com/presentation/d/1o_E5OXvwxlfGWA6acCmPZSfqdcXLcchXoi6uKUs8v0k/pub?start=false&loop=false&delayms=3000&slide=id.p)

[Second](https://docs.google.com/presentation/d/1Slp6kZGeUP5h8ExSkbrzxxrDx7O15Ih8KKJh_UIm9_o/pub?start=false&loop=false&delayms=3000&slide=id.p)

##### Справочники
[developer.mozilla.org](https://developer.mozilla.org/uk/docs/Web/JavaScript)

[learn.javascript.ru](https://learn.javascript.ru/getting-started)

##### Стиль кода
https://learn.javascript.ru/coding-style

##### Ссылки из презентации
[Пример из vanilla js](http://vanilla-js.com/)

[Метод call()](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Function/call)

[Метод apply()](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Function/apply)

[Метод bind()](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Function/bind)

[this](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Operators/this)

[call & apply](https://learn.javascript.ru/call-apply)

[bind и карринг](https://learn.javascript.ru/bind#%D0%BA%D0%B0%D1%80%D1%80%D0%B8%D0%BD%D0%B3)

[document.write.3](https://puzzleweb.ru/javascript/examples/3-4.php)
[document.write.4](https://www.w3schools.com/jsref/met_doc_write.asp)

[JavaScript Events](https://www.w3schools.com/js/js_events.asp)

[Events. Full list_1](https://developer.mozilla.org/en-US/docs/Web/Events)
[Events. Full list_2](https://www.w3schools.com/jsref/dom_obj_event.asp)

[Events. Пример 1](https://www.w3schools.com/jsref/tryit.asp?filename=tryjsref_onclick)
[Events. Пример 2](https://www.w3schools.com/js/tryit.asp?filename=tryjs_events2)

[Свойства и методы Event](https://developer.mozilla.org/en-US/docs/Web/API/Event)

[Всплытие и перехват пример](https://jsfiddle.net/ilia/yuktekwd/4/)

[Events and Interfaces](https://learn.javascript.ru/events-and-interfaces)

[Введение в события DOM](https://frontender.info/an-introduction-to-dom-events/)
[Введение в события DOM. Пример](http://jsbin.com/valasomeli/edit?html,js,output)

[Events bubbling и events capturing](https://habrahabr.ru/post/126471/)

[Создание и вызов событий](https://developer.mozilla.org/ru/docs/Web/Guide/Events/%D0%A1%D0%BE%D0%B7%D0%B4%D0%B0%D0%BD%D0%B8%D0%B5_%D0%B8_%D0%B2%D1%8B%D0%B7%D0%BE%D0%B2_%D1%81%D0%BE%D0%B1%D1%8B%D1%82%D0%B8%D0%B9)
[Пример 1](https://codepen.io/iliatcymbal/pen/jqqgaW?editors=0010)
[Пример 2](https://codepen.io/iliatcymbal/pen/JXXgXr?editors=1010)

[FocusEvent](https://developer.mozilla.org/en-US/docs/Web/API/FocusEvent/FocusEvent)
[MouseEvent](https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/MouseEvent)
[WheelEvent](https://developer.mozilla.org/en-US/docs/Web/API/WheelEvent/WheelEvent)
[KeyboardEvent](https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/KeyboardEvent)
[CompositionEvent](https://developer.mozilla.org/en-US/docs/Web/API/CompositionEvent)

[Пример. Custom Event](https://codepen.io/iliatcymbal/pen/aNNeQY?editors=1010)

[Резюме events](https://learn.javascript.ru/dispatch-events#%D0%B8%D1%82%D0%BE%D0%B3%D0%BE)


