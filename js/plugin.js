// this. Задачи

// --------------------------------------------------------------------------------------------------------------
// 1. Создать объект, который описывает ширину и высоту прямоугольника, а также может посчитать площадь фигуры:
//    const rectangle = {width:..., height:..., getSquare:...};
// --------------------------------------------------------------------------------------------------------------
const rectangle = {
    width: 100,
    height: 50,
    getSquare: function () {
        return this.width * this.height;
    }
};

console.log(rectangle.getSquare()); // 5000

// --------------------------------------------------------------------------------------------------------------
// 2. Создать объект, у которого будет цена товара и его скидка, а также два метода:
//     для получения цены и для расчета цены с учтеом скидки:
// const price = {
//     price: 10,
//     discount: '15%',
//     ...
// };
// price.getPrice(); // 10
// price.getPriceWithDiscount(); // 8.5
// --------------------------------------------------------------------------------------------------------------
const price = {
    price: 10,
    discount: '15%',
    getPrice: function () {
        return this.price;
    },
    getPriceWithDiscount: function () {
        return this.price - this.price * parseFloat(this.discount) / 100;
    }
};

console.log(price.getPrice()); // 10
console.log(price.getPriceWithDiscount()); // 8.5

// --------------------------------------------------------------------------------------------------------------
// 3. Дан объект и функция:
// const user = { name: 'Abraham'},
//       getUserName = function() {...};
// Внесите в этот код такие изменения, чтобы можно было вызывать
// user.getName() и получить 'Abraham'
// --------------------------------------------------------------------------------------------------------------
const user3 = {
    name: 'Abraham',
    getName: getUserName
};

function getUserName() {
    return this.name;
}

console.log(user3.getName()); // 'Abraham'

// --------------------------------------------------------------------------------------------------------------
// 4. Создать объект, у которого будет поле высоты и метод "увеличить высоту на один".
//    Метод должен возвращать новую высоту:
// object.height = 10;
// object.inc(); // придумать свое название для метода
// object.height; // 11;
// --------------------------------------------------------------------------------------------------------------
let object = {
    height: 10,
    incHeight: function () {
        return ++this.height
    }
};

console.log(object.height); // 10
console.log(object.incHeight()); // 11
console.log(object.height); // 11

// --------------------------------------------------------------------------------------------------------------
// 5. Создать объект "вычислитель", у которого есть числовое свойство "значение" и методы "удвоить, "прибавить один",
//    "отнять один". Методы можно вызывать через точку, образуя цепочку методов:
//
// const numerator = {
//     value: 1,
//     double: function() {...},
//     plusOne: function() {...},
//     minusOne: function() {...},
// }
// numerator.double().plusOne().plusOne().minusOne();
// numerator.value // 3
// --------------------------------------------------------------------------------------------------------------
const numerator = {
    value: 1,
    double: function() {
        this.value *= 2;
        return this;
    },
    plusOne: function() {
        ++this.value;
        return this;
    },
    minusOne: function() {
        --this.value;
        return this;
    },
};

numerator
    .double()
    .plusOne()
    .plusOne()
    .minusOne();

console.log(numerator.value); // 3

// --------------------------------------------------------------------------------------------------------------
// 6. Разобрать и объяснить, что тут происходит
// --------------------------------------------------------------------------------------------------------------
const user = {name: 'Abraham'},
    otherUser = {
        name: 'John',
        getName: function () {
            return this.name;
        }
    };

// undefined потому, что у этого обьекта нет метода getName
user.getName; // undefined

// объект user "одалживает" метод getName у объекта otherUser
user.getName = otherUser.getName;

// получаем 'Abraham' потому что выше мы произвели одалживание метода. Т.е. у обьекта user тоже появился
// метод getName и мы можем его вызвать.
user.getName(); // 'Abraham'

// вызываем метод getName у обьекта otherUser
otherUser.getName(); // 'John'

// --------------------------------------------------------------------------------------------------------------
// 1. Что выведет код, почему?
// --------------------------------------------------------------------------------------------------------------
function getList() {
    return this.list;
}

let users = {
    length: 4,
    list: ['Abraham', 'James', 'John', 'Steven']
};

// т.к. у глобального объекта window нет свойства list
getList(); // undefined

users.getList = getList;

// выведет свойство list объекта users, т.е. массив с данными, т.к. выше мы создаем метод getList
users.getList(); // (4) ["Abraham", "James", "John", "Steven"]

// то же самое что и выше, т.к. принудительно задаем контекст вызова
getList.call(users); // (4) ["Abraham", "James", "John", "Steven"]

// --------------------------------------------------------------------------------------------------------------
// 2. Создать объект с розничной ценой и количеством продуктов.
//    Этот объект должен содержать метод для получения общей стоимости всех товаров (цена * количество продуктов)
// --------------------------------------------------------------------------------------------------------------
let products = {
    retailPrice: 500,
    quantity: 27,
    getTotalPrice: function () {
        return this.retailPrice * this.quantity
    }
};

console.log(products.getTotalPrice()); // 13500

// --------------------------------------------------------------------------------------------------------------
// 3. Создать объект из предыдущей задачи.
//    Создать второй объект, который описывает количество деталей и цену за одну деталь.
//    Для второго объекта нужно узнать общую стоимость всех деталей, но нельзя создавать новые функции и методы.
//    Для этого "позаимствуйте" метод из предыдущего объекта.
// --------------------------------------------------------------------------------------------------------------
let product = {
    retailPrice: 10,
    quantity: 10
};

product.getTotalPrice = products.getTotalPrice;

console.log(product.getTotalPrice()); // 100

// --------------------------------------------------------------------------------------------------------------
// 4. Даны объект и функция:
//    Не изменяя функцию или объект, получить результат функции getSquare для объекта sizes.
// --------------------------------------------------------------------------------------------------------------
let sizes = {
        width: 5,
        height: 10
    },
    getSquare = function () {
        return this.width * this.height
    };

console.log(getSquare.call(sizes)); // 50

// --------------------------------------------------------------------------------------------------------------
// 5. Дан массив let numbers = [4, 12, 0, 10, -2, 4].
//    Используя ссылку на массив numbers и Math.min, найти минимальный элемент массива.
// --------------------------------------------------------------------------------------------------------------
let numbers = [4, 12, 0, 10, -2, 4],
    min = Math.min.apply(null, numbers);

console.log(min); // -2

// --------------------------------------------------------------------------------------------------------------
// 6. Исправить метод getFullHeight таким образом,
//    чтобы можно было вычислить сумму трех слагаемых (высота плюс отступы).
//    Для другого объекта block {height: '5px', marginTop: '3px', marginBottom: '3px'} вычислить полную высоту
//    getGullHeight, используя для этого объект element. В объект ничего не дописывать.
//
// const element = {
//     height: '15px',
//     marginTop: '5px',
//     marginBottom: '5px',
//     getFullHeight: function () {
//         return this.height + this.marginTop + this.marginBottom; // эта часть с ошибкой
//     }
// };
// --------------------------------------------------------------------------------------------------------------

const element ={
    height: '15px',
    marginTop: '5px',
    marginBottom: '5px',
    getFullHeight: function () {
        return `Высота элемента равна ${parseInt(this.height) + parseInt(this.marginTop) + parseInt(this.marginBottom)}px`
    }
};

const block = {
    height: '5px',
    marginTop: '3px',
    marginBottom: '3px'
};

console.log(element.getFullHeight.call(block)); // Высота элемента равна 11px

// --------------------------------------------------------------------------------------------------------------
// 7. Измените функцию getElementHeight таким образом, чтобы можно было вызвать getElementHeight() и получить 25.
// let element = {
//     height: 25,
//     getHeight: function () {
//         return this.height;
//     }
// };
//
// let getElementHeight = element.getHeight;
// getElementHeight(); // undefined
// --------------------------------------------------------------------------------------------------------------
let element7 = {
    height: 25,
    getHeight: function () {
        return this.height
    }
};

let getElementHeight = element7.getHeight.bind(element7);

console.log(getElementHeight()); // 25


// Лексическо окружение, задачи

// --------------------------------------------------------------------------------------------------------------
// 1. Что выведет следующий код? Почему?
//
// getBigName(userName);
//
// function getBigName(name) {
//     name = name + '';
//     return name.toUpperCase();
// }
//
// var userName = 'Alex';
// --------------------------------------------------------------------------------------------------------------

// Переменная userName хоть и существует, но на момент запуска функции она равна undefined
console.log(getBigName(userName)); // UNDEFINED

function getBigName(name) {
    name = name + '';
    return name.toUpperCase();
}

var userName = 'Alex';

// --------------------------------------------------------------------------------------------------------------
// 2. Какое значение вернет функция test? Почему?
//
// function test() {
//     var name = 'Alex';
//     return getBigName(userName)
// }
//
// function getBigName(name) {
//     name = name + '';
//     return name.toUpperCase();
// }
//
// var userName = 'Ivan';
// test();
// --------------------------------------------------------------------------------------------------------------
function test() {
    var name = 'Alex';
    return getBigName(userName)
}

function getBigName(name) {
    name = name + '';
    return name.toUpperCase();
}

var userName = 'Ivan';

// Т.к. переменная userName определяется до вызова функции test(), которая передает userName в функцию getBigName и
// возвращает результат выполнения этой функции
console.log(test()); // IVAN

// --------------------------------------------------------------------------------------------------------------
// 3. Что выведет функция getFood? Почему?
//
// var food = 'cucumber';
//
// (function () {
//     var food = 'bread';
//     getFood();
// })();
//
// function getFood() {
//     console.log(food);
// }
// --------------------------------------------------------------------------------------------------------------

// LexicalEnvironment0
// {
//     food: 'cucumber',
//     getFood: function...,
//     scope: null
// }
var food = 'cucumber';

(function () {
    // LexicalEnvironment1
    // {
    //     food: 'bread',
    //     args: arguments,
    //     getFood: ...,
    //     scope: LexicalEnvironment0
    // }
    var food = 'bread';
    getFood();
})();

function getFood() {
    // LexicalEnvironment1
    // {
    //     food: 'cucumber',
    //     args: arguments,
    //     scope: LexicalEnvironment0
    // }
    console.log(food); // cucumber
}


// Замыкание. Задачи

// --------------------------------------------------------------------------------------------------------------
// 1. Какое значение вернет функция getDollar? Почему?
//
// var dollar,
//     getDollar;
//
// (function () {
//     var dollar = 0;
//     getDollar = function () {
//         return dollar
//     }
// }());
//
// dollar = 30;
// getDollar();
// --------------------------------------------------------------------------------------------------------------

var dollar,
    getDollar;

(function () {
    var dollar = 0;
    getDollar = function () {
        return dollar
    }
}());

dollar = 30;
console.log(getDollar());
// вернется 0, потому что функция getDollar() использует переменную dollar, объяленную в родительской функции.

// --------------------------------------------------------------------------------------------------------------
// 2. Что будет выведено в консоль? Почему?
//
// var greet = 'hello';
// (function () {
//   var text = 'world';
//   console.log(greet + text);
// }());
// console.log(greet + text);
// --------------------------------------------------------------------------------------------------------------
var greet = 'hello';

(function () {
  var text = 'world';
  console.log(greet + text); // helloworld - потому что замыкание
}());

// console.log(greet + text); // Uncaught ReferenceError: text is not defined. потому что нет доступа к переменной text

// --------------------------------------------------------------------------------------------------------------
// 3. Создайте функцию которая б умела делать:
// minus(10)(6); // 4
// minus(5)(6); // -1
// minus(10)(); // 10
// minus()(6); // -6
// minus()(); // 0
// --------------------------------------------------------------------------------------------------------------
function minus(num1 = 0) {
    return function (num2 = 0) {
        return num1 - num2;
    }
}

console.log(minus(10)(6)); // 4
console.log(minus(5)(6)); // -1
console.log(minus(10)()); // 10
console.log(minus()(6)); // -6
console.log(minus()()); // 0

// --------------------------------------------------------------------------------------------------------------
// 4. Реализовать функцию, которая умножает и умеет запоминать возвращаемый результат между вызовами:
// function MultiplyMaker ...
// const multiply = MultiplyMaker(2);
// multiply(2); // 4 (2 * 2)
// multiply(1); // 4 (4 * 1)
// multiply(3); // 12 (4 * 3)
// multiply(10); // 120 (12 * 10)
// --------------------------------------------------------------------------------------------------------------
function MultiplyMaker(number) {
    return function (value) {
        if (isNaN(value)) {
            return new Error('Аругмент должен быть числом')
        } else {
            number *= value;
        }

        return number
    }
}

const multiply = MultiplyMaker(2);

console.log(multiply(2)); // 4 (2 * 2)
console.log(multiply(1)); // 4 (4 * 1)
console.log(multiply(3)); // 12 (4 * 3)
console.log(multiply(10)); // 120 (12 * 10)

// --------------------------------------------------------------------------------------------------------------
// 5. Реализовать модуль, который работает со строкой и имеет методы:
//    a. установить строку
//       i. если передано пустое значение, то установить пустую строку
//      ii. если передано число, число привести к строке
//    b. получить строку
//    c. получить длину строки
//    d. получить строку-перевертыш
//
//    Пример:
//    модуль.установитьСтроку('abcde');
//    модуль.получитьСтроку(''); // 'abcde'
//    модуль.получитьДлину(); // 5
// --------------------------------------------------------------------------------------------------------------
const module = (function () {
    let string = '';

    function setString(val = '') {
        string = val + '';
    }

    function getString() {
        return string
    }

    function getStringLength() {
        return string.length
    }

    function getReverseString() {
        return string.split('').reverse().join('')
    }

    return {
        setString,
        getString,
        getStringLength,
        getReverseString
    }
}());

module.setString('abcde');
console.log(module.getString('')); // 'abcde'
console.log(module.getStringLength()); // 5
console.log(module.getReverseString()); // edcba

// --------------------------------------------------------------------------------------------------------------
// 6. Создайте модуль "калькулятор", который умеет складывать, умножть, вычитать, делить и возводить в степен.
//    Конечное значение округлить до двух знаков после точки (значение должно храниться в обычной переменной, не в this)
//
//    модуль.установитьЗначение(10); // значение = 10
//    модуль.прибавить(5); // значение += 5
//    модуль.умножить(2); // значение *= 2
//    модуль.узнатьЗначение(); // вывести в консоль 30 (здесь надо округлить)
//
//    Также можно вызывать методы цепочкой:
//    модуль.установитьЗначение(10).вСтепень(2).узнатьЗначение(); // 100
// --------------------------------------------------------------------------------------------------------------
const calculator = (function () {
    let result;

    function setCount(value) {
        result = parseFloat(value);
        return this
    }

    function getSum(value) {
        result += value;
        return this
    }

    function getMultiply(value) {
        result *= value;
        return this
    }

    function getMinus(value) {
        result -= value;
        return this
    }

    function getExp(value) {
        if (value > 1 ) {
            for (let i = 1; i < value; i++) {
                result *= result
            }
        }

        return this
    }

    function getCount() {
        if (isNaN(result)) {
            return new Error('Аругмент должен быть числом')
        } else {
            result.toFixed(2);
            return result
        }
    }

    return{
        setCount,
        getSum,
        getMultiply,
        getMinus,
        getExp,
        getCount
    }
}());

calculator.setCount(10);
console.log(calculator.getSum(10).getCount()); // 20 (10 + 10)
console.log(calculator.getMultiply(5).getCount()); // 100 (20 * 5)
console.log(calculator.getMinus(10).getCount()); // 90 (100 - 10)
console.log(calculator.getExp(3).getCount()); // 8100 (90 ^ 2)

console.log(calculator.setCount(10).getSum(2).getMultiply(5).getCount()); // 60 ((10 + 2) * 5)
